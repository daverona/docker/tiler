FROM ubuntu:18.04

ENV LANG=C.UTF-8
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
  && apt-get install --yes --quiet --no-install-recommends \
    libvips-tools \
    python3 \
    python3-pip \
    python3-setuptools \
    tzdata \
  && apt-get clean && rm -rf /var/lib/apt/lists/* \
  && python3 -m pip install --no-cache-dir --upgrade pip \
  && pip3 install --no-cache-dir \
    dotty_dict \
    xmltodict

COPY tile.sh /bin/tile
COPY tiledzijson.py /bin/tiledzijson
COPY tilemetajson.py /bin/tilemetajson
RUN chmod a+x /bin/tile*
COPY README.md /data/
WORKDIR /data
