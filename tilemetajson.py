#!/usr/bin/env python3

import argparse
import json
import os
import re
import sys
from dotty_dict import dotty


def canonicalize_key(key):
    return re.sub(r'([^\.]+)\[(\d+)\]', r'\1.\2', key)


def canonicalize_val(val):
    if re.match(r'[-+]?\d+', val) is not None:
        try:
            return int(val)
        except:
            pass
    if re.match(r'[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?', val) is not None:
        try:
            return float(val)
        except:
            pass
    return val


def read_header_info(path):
    stream = open(path, 'r') if path else sys.stdin
    header = {}
    for index, line in enumerate(stream):
        k, v = line.rstrip('\r\n').split(': ', 1)
        header[k] = v
    if stream is not sys.stdin:
        stream.close()
    return header


def make_python_dict(header):
    pydict = dotty()
    kv_list = list(header.items())
    for k, v in kv_list[1:]:
        k = canonicalize_key(k)

        # exception rules for values:
        if k == 'filename':
            v = v.strip('"')

        pydict[k] = canonicalize_val(v)
    return {
        kv_list[0][0]: kv_list[0][1],
        **dict(pydict)
    }


def write_json(path, pydict):
    stream = open(path, 'w') if path else sys.stdout
    json.dump(pydict, stream, indent=2)
    if stream is not sys.stdout:
        stream.close()


def parse():
    parser = argparse.ArgumentParser(description='Convert vipsheader output to JSON format')
    parser.add_argument('--infile', metavar='INFILE', type=str, dest='infile', help='designate a vipsheader output filename (default: stdin)')
    parser.add_argument('--outfile', metavar='OUTFILE', type=str, dest='outfile', help='designate JSON output filename (default: stdout)')
    args = parser.parse_args()
    return args


def main():
    args = parse()
    header = read_header_info(args.infile)
    pydict = make_python_dict(header)
    write_json(args.outfile, pydict)


if __name__ == '__main__':
    main()
