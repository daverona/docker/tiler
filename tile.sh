#!/bin/bash
set -e
umask 022

echo "Making tile images"
prefix=${1%%.*}
vips dzsave --vips-concurrency=$(nproc) "$1" "$prefix"
cat "$prefix.dzi" | tiledzijson > "$prefix.dzi.json"

echo "Making thumbnail image"
set +e
vipsthumbnail --vips-concurrency=$(nproc) --size=200x --output=%s.thumbnail.jpeg "$1"[level=2]
set -e
if [ $? -ne 0 ]; then
  \rm -rf "$prefix.thumbnail.jpeg"
  vipsthumbnail --vips-concurrency=$(nproc) --size=200x --output=%s.thumbnail.jpeg "$1"
fi

echo "Retrieving header info"
vipsheader -a "$1" | tilemetajson > "$prefix.meta.json"
