#!/usr/bin/env python3

import argparse
import json
import sys
import xmltodict


def read_xml(path):
    stream = open(path, 'r') if path else sys.stdin
    obj = xmltodict.parse(stream.read())
    if stream is not sys.stdin:
        stream.close()
    return obj


def sanitize_keys(obj):
    sanitized = {}
    for k, v in obj.items():
        if isinstance(v, dict):
            v = sanitize_keys(v)

        # exception rules for keys
        if k.startswith('@') and len(k) >= 2:
            sanitized[k[1:]] = v
        else:
            sanitized[k] = v

    return sanitized


def write_json(path, obj):
    stream = open(path, 'w') if path else sys.stdout
    json.dump(obj, stream, indent=2)
    if stream is not sys.stdout:
        stream.close()


def parse():
    parser = argparse.ArgumentParser(description='Convert DZI in XML format to JSON format')
    parser.add_argument('--infile', metavar='INFILE', type=str, dest='infile', help='designate XML input filename (default: stdin)')
    parser.add_argument('--outfile', metavar='OUTFILE', type=str, dest='outfile', help='designate JSON output filename (default: stdout)')
    args = parser.parse_args()
    return args


def main():
    args = parse()
    obj = read_xml(args.infile)
    obj = sanitize_keys(obj)
    write_json(args.outfile, obj)


if __name__ == '__main__':
    main()
